#include "adjarray.h"

adjarray_t* load_graph(const char* fname) {

	//Open the graph file
	FILE* f = fopen(fname, "r");

	/*
		n = Number of nodes
		u, v = Edge in the graph file
		e = number of edges read
		m = size of edges array
		edges = store the edges
	*/
	uint32_t u, v, n = 0, e = 0;
	edge_t* edges = (edge_t*) malloc(sizeof(edge_t) * NLINKS);
	uint32_t m = NLINKS;

	//Read graph file
	while (fscanf(f, "%u %u", &u, &v) == 2) {
		/*if (e == m) {
			m += NLINKS/10;
			edges = (edge_t*) realloc(edges, sizeof(edge_t) * m);
		}*/
		edges[e].src = u;
		edges[e].dst = v;
		if (n < u) n = u;
		if (n < v) n = v;
		e++;
	}

	//+1 to n, because the first node is 0
	n++;

	//allocate, init and calculate the cd array
	uint32_t* cd = (uint32_t*) calloc(n+1, sizeof(uint32_t));
	uint32_t* cd_in = (uint32_t*) calloc(n+1, sizeof(uint32_t));

	for (uint32_t i = 0; i < e; cd[edges[i].src + 1]++, i++);
	for (uint32_t i = 1; i < n+1; cd[i] += cd[i-1], i++);

	for (uint32_t i = 0; i < e; cd_in[edges[i].dst + 1]++, i++);
	for (uint32_t i = 1; i < n+1; cd_in[i] += cd_in[i-1], i++);

	//allocate and calculate adj and tran
	uint32_t* adj = (uint32_t*) malloc(sizeof(uint32_t) * e);
	double* tran = (double*) malloc(sizeof(double) * e);
	for (uint32_t i=0; i<e; i++) {
		u = edges[i].src;
		adj[i] = edges[i].dst;
		tran[i] = 1.0 / (cd[u+1] - cd[u]);
	}

	//Free edges array
	free(edges);

	//allocate an adjarray_t and complete it
	adjarray_t* G = (adjarray_t*) malloc(sizeof(adjarray_t));
	G->n = n;
	G->cd = cd;
	G->cd_in = cd_in;
	G->adj = adj;
	G->tran = tran;
	return G;
}

void del_graph(adjarray_t* G) {
	free(G->cd);
	free(G->adj);
	free(G->tran);
	free(G);
}

uint32_t idx_uv(adjarray_t*G, uint32_t u, uint32_t v) {
	/*//Linear search O(n)
	for (uint32_t i=G->cd[u]; i<G->cd[u+1]; i++) {
		if (G->adj[i] == v) return i;
	}*/

	//Dichotomic search O(log(n))
	uint32_t begin = G->cd[u];
	uint32_t end = G->cd[u+1]-1;
	uint32_t middle;
	while (begin < end) {
		middle = (begin + end) / 2;
		if (v == G->adj[middle]) return middle;
		if (G->adj[middle] > v ) end = middle - 1;
		else begin = middle + 1;
	}
	if (v == G->adj[begin]) return begin;
	return NOT_FOUND(G);
}

double tran_uv(adjarray_t* G, uint32_t u, uint32_t v) {
	if (G->cd[u+1] - G->cd[u] == 0) return 1.0 / G->n;
	uint32_t idx = idx_uv(G, u, v);
	if (idx == NOT_FOUND(G)) return 0;
	return G->tran[idx];
}

void prod_mat_vect(adjarray_t* G, double* P, double* R) {

	for (uint32_t u=0; u<G->n; R[u]=0, u++);

	uint32_t v;
	double som = 0, _1_n = 1.0/G->n;
	for (uint32_t u=0; u<G->n; u++) {
		if (G->cd[u+1] - G->cd[u] == 0) {
			som += _1_n * P[u];
		} else {
			for (uint32_t v=G->cd[u]; v<G->cd[u+1]; v++) {
				R[G->adj[v]] += G->tran[v] * P[u];
			}
		}
	}

	for (uint32_t u=0; u<G->n; u++) {
		R[u] += som;
		if (G->cd[u+1] - G->cd[u] == 0) {
			R[u] -= _1_n * P[u];
		}
	}

}

double normalize2(adjarray_t* G, double* P) {
	double s = 0;
	for (uint32_t u=0; u<G->n; u++) {
		s += pow(P[u], 2);
	}
	s = sqrt(s);
	for (uint32_t u=0; u<G->n; u++) {
		P[u] += (1 - s) / G->n;
	}
	return s;
}

double* page_rank(adjarray_t* G, double alpha, int t) {
	//Allocate and init P (= I)
	double* P = (double*) malloc(sizeof(double) * G->n);
	double* R = (double*) malloc(sizeof(double) * G->n);
	double* tmp;
	double _1_n = 1.0/G->n;
	for (uint32_t u=0; u<G->n; P[u] = _1_n, u++);

	for (int i=0; i<t; i++) {
		printf("For t = %d\n", i+1);
		prod_mat_vect(G, P, R);

		for (uint32_t u=0; u<G->n; u++) {
			R[u] = (1 - alpha) * R[u] + alpha * _1_n;
		}

		normalize2(G, R);

		tmp = P;
		P = R;
		R = tmp;

	}

	free(R);

	return P;
}