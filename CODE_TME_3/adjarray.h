#ifndef __ADJARRAY_H__
#define __ADJARRAY_H__

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>

#define NLINKS 50000000
#define NOT_FOUND(G) G->cd[G->n]

typedef struct {
	uint32_t src;
	uint32_t dst;
} edge_t;

typedef struct {
	uint32_t n;
	uint32_t *cd;
	uint32_t* cd_in;
	uint32_t *adj;
	double* tran;
} adjarray_t;

adjarray_t* load_graph(const char* fname);
void del_graph(adjarray_t* G);
uint32_t idx_uv(adjarray_t*G, uint32_t u, uint32_t v);
double tran_uv(adjarray_t* G, uint32_t u, uint32_t v);
double* page_rank(adjarray_t* G, double alpha, int t);
#endif
