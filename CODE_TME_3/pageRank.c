#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <string.h>
#include "adjarray.h"

int main(int argc, char** argv) {

  if (argc != 2) {
    printf("./pageRank file_name\n");
    exit(EXIT_FAILURE);
  }

  clock_t start, end;
  double cpu_time_used;

  printf("Start loading ...\n");
  fflush(stdout);
  adjarray_t* G = load_graph(argv[1]);

  printf("Start pageRank ...");
  start = clock();
  double* P = page_rank(G, 0.15, 40);
  end = clock();
  cpu_time_used = end - start;
  printf("End pageRank\nCPU time used = %lf\n", cpu_time_used/ CLOCKS_PER_SEC);

  del_graph(G);

  return 0;
}