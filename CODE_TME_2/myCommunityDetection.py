# -*- coding: utf-8 -*-

def communityDetection(G, dmin):
    
    C = {}
    E = copy(G.nodes)
    
    while E not empty:
        u = random node in E
        Delete u from E
        c = {u}
        Queue <-- u
        mark(u)
        while Queue not empty:
            n <-- Queue
            for each v in neighboors(n):
                if not marked(v):
                    maxi = 0
                    for each w in c:
                        maxi = max(maxi, |neighboors(w) inter neighboors(v)|)
                        if maxi >= dmin:
                            Queue <-- v
                            Add v to c
                            Delete v from E
                            break
        Add c to C
    
    return C

    