#ifndef __MYGRAPH_H__
#define __MYGRAPH_H__

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>

#define NLINKS 50000000
#define NOT_FOUND(G) G->cd[G->n]

typedef struct {
	uint32_t u1;
	uint32_t u2;
} edge_t;

typedef struct {
	uint32_t n;
	uint32_t *cd;
	uint32_t *adj;
} adjarray_t;

adjarray_t* load_graph(const char* fname);
void del_graph(adjarray_t* G);

#endif
