#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "myGraph.h"

#define MAX_COLOR 0xFFFFFF

uint32_t* labelPropagation(adjarray_t* G) {

    // Step 1 : Give a unique label to each node in the network
    uint32_t* labels = (uint32_t*) malloc(sizeof(uint32_t) * G->n);
    for (uint32_t u=0; u<G->n; labels[u]=u, u++);

    // n-array to save the nodes orders
    uint32_t* nodes = (uint32_t*) malloc(sizeof(uint32_t) * G->n);
    for (uint32_t u=0; u<G->n; nodes[u]=u, u++);
    uint32_t v, w;

    // n-array to count the label occurring of u neighboors
    uint32_t* counts = (uint32_t*) calloc(G->n, sizeof(uint32_t));
    uint32_t* idx_counts = (uint32_t*) malloc(G->n * sizeof(uint32_t));
    uint32_t idx;

    // Loop stop condition
    int stop = 0, nbre = 1;
    clock_t start, end;
    double cpu_time_used;

    while (!stop) {
        start = clock();
        stop = 1;

        // Step 2 : Arrange the nodes in the network in a random order
        for (uint32_t u=0; u<G->n; u++) {
            v = rand() % G->n;
            if (u != v) {
                w = nodes[u];
                nodes[u] = nodes[v];
                nodes[v] = w;
            }
        }

        // Step 3 : for each node in the network (in this random order)
        // set its label to a label occurring with the highest frequency
        // among its neighboors
        uint32_t labelMax, countMax;

        for (uint32_t u=0, idx=0; u<G->n; idx=0, u++) {

            for (uint32_t v=G->cd[nodes[u]]; v<G->cd[nodes[u]+1]; v++) {
                if (counts[labels[G->adj[v]]] == 0) {
                    idx_counts[idx++] = labels[G->adj[v]];
                }
                counts[labels[G->adj[v]]]++;
            }

            for (uint32_t v=0, countMax=0; v<idx; v++) {
                if (countMax < counts[idx_counts[v]]) {
                    countMax = counts[idx_counts[v]];
                    labelMax = idx_counts[v];
                }
                counts[idx_counts[v]] = 0;
            }

            if (labelMax != labels[nodes[u]]) {
                labels[nodes[u]] = labelMax;
                stop = 0;
            }

        }

        end = clock();
        cpu_time_used = end - start;
        printf("Tour numero : %u\n", nbre++);
        printf("CPU time used = %lf\n", cpu_time_used/ CLOCKS_PER_SEC);

    }

    free(nodes);
    free(counts);
    free(idx_counts);

    return labels;
}

void communitiesViz(adjarray_t* G, uint32_t* labels, const char* fname) {

    FILE* file = fopen(fname, "w");

    //Give a uniq color for each community
    uint32_t* colors = (uint32_t*) malloc(sizeof(uint32_t) * G->n);
    for (uint32_t u=0; u<G->n; colors[u]=labels[u]*MAX_COLOR/G->n, u++);

    char* draw = (char*) calloc(G->n, sizeof(char));

    fprintf(file, "graph G{splines=line;node[shape=point,width=1];");

    for (uint32_t u=0; u<G->n ;u++) {
        //Si le graph est enorme
        if (!draw[labels[u]]) {
            fprintf(file, "%u[color=\"#%.6x\"];", u, colors[u]);
            draw[labels[u]] = 1;
        }
        //Si la taille de graphe est petite
        //fprintf(file, "%u[color=\"#%.6x\"];", u, colors[u]);
    }

    //Si la taille de graphe est petite, si c'est grand on le met en commentaire
    /*for (uint32_t u=0; u<G->n; u++) {
        for (uint32_t v=G->cd[u]; v<G->cd[u+1]; v++) {
            if (u < G->adj[v]) {
                if (colors[u] == colors[G->adj[v]]) {
                    fprintf(file, "%u--%u[color=\"#%.6x\"];", u, G->adj[v], colors[u]);
                } else {
                    fprintf(file, "%u--%u[color=white];", u, G->adj[v]);
                }
            }
        }
    }*/

    fprintf(file, "}");

    free(colors);
    fclose(file);
}


int main(int argc, char** argv) {

    srand(time(NULL));

    if (argc < 3) {
        perror("./labelPropagation nom_du_fichier1 nom_du_fichier2 nom_du_fichier3");
        exit(EXIT_FAILURE);
    }

    adjarray_t* G = load_graph(argv[1]);
    printf("Load success ...\n");

    uint32_t* labels = labelPropagation(G);

    communitiesViz(G, labels, argv[2]);

    /*FILE* f = fopen(argv[3], "w");
    for (uint32_t u=0; u<G->n; u++) {
        fprintf(f, "%u\n", labels[u]);
    }

    fclose(f);*/

    free(labels);
    del_graph(G);
    return 0;
}

/*
for (uint32_t u=0; u<G->n; u++) {
    printf("");
}
*/