#include "myGraph.h"

adjarray_t* load_graph(const char* fname) {

	FILE* f = fopen(fname, "r");

	uint32_t u, v, n = 0, e = 0;
	edge_t* edges = (edge_t*) malloc(sizeof(edge_t) * NLINKS);
	uint32_t m = NLINKS;

	while (fscanf(f, "%u %u", &u, &v) == 2) {
		if (e == m) {
			m += NLINKS/10;
			edges = (edge_t*) realloc(edges, sizeof(edge_t) * m);
		}
		edges[e].u1 = u;
		edges[e].u2 = v;
		if (n < u) n = u;
		if (n < v) n = v;
		e++;
	}

	n++;

	uint32_t* cd = (uint32_t*) calloc(n+1, sizeof(uint32_t));

	for (uint32_t i = 0; i < e; i++) {
		cd[edges[i].u1 + 1]++;
		cd[edges[i].u2 + 1]++;
	}
	for (uint32_t i = 1; i < n+1; cd[i] += cd[i-1], i++);

	/*for (uint32_t i=0; i<e; i++) {
		printf("(%u,%u)\n", edges[i].u1, edges[i].u2);
	}*/

	uint32_t* d = (uint32_t*) calloc(n, sizeof(uint32_t));
	uint32_t* adj = (uint32_t*) malloc(cd[n] * sizeof(uint32_t));

	for (uint32_t i=0; i<e; i++) {
		u = edges[i].u1;
		v = edges[i].u2;
		adj[cd[u]+d[u]++] = v;
		adj[cd[v]+d[v]++] = u;
	}

	free(edges);

	adjarray_t* G = (adjarray_t*) malloc(sizeof(adjarray_t));
	G->n = n;
	G->cd = cd;
	G->adj = adj;
	return G;
}

void del_graph(adjarray_t* G) {
	free(G->cd);
	free(G->adj);
	free(G);
}
