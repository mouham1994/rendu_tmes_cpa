# -*- coding: utf-8 -*-

from random import random
from networkx import Graph, draw

def simple_bechmark(p, q):
    g = Graph()
    for i in range(0, 399):
        x = i//100
        for j in range(i+1, 400):
            y = j//100
            if (x == y):
                if (random() < p):
                    g.add_edge(i, j)
                    print(i, j)
            else:
                if (random() < q):
                    g.add_edge(i, j)
                    print(i, j)
    return g

"""
#p = 1, q = 0
g = simple_bechmark(1, 0)
draw(g)

#p = 0.95, q = 0.05
g = simple_bechmark(0.95, 0.05)
draw(g)

#p = 0.9, q = 0.1
g = simple_bechmark(0.9, 0.1)
draw(g)

#p = 0.85, q = 0.15
g = simple_bechmark(0.85, 0.15)
draw(g)

#p = 0.8, q = 0.2
g = simple_bechmark(0.8, 0.2)
draw(g)

#p = 0.75, q = 0.25
g = simple_bechmark(0.75, 0.25)
draw(g)
"""