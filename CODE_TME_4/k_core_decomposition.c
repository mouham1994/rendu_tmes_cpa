#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "myGraph.h"

uint32_t find_min(adjarray_t* G, char* mark_vertex) {

    uint32_t* position = G->heap->position;
    uint32_t* vertex = G->heap->vertex;
    uint32_t* degree = G->heap->degree;
    uint32_t size = G->heap->size;

    uint32_t min = vertex[1];
    uint32_t i = 1;

    while (2*i < size) {
        if (2*i+1 < size) { //Two sons

            if (degree[vertex[2*i]] < degree[vertex[2*i+1]]) {
                vertex[i] = vertex[2*i];
                position[vertex[2*i]] = i;
                i = 2*i;
            } else {
                vertex[i] = vertex[2*i+1];
                position[vertex[2*i+1]] = i;
                i = 2*i+1;
            }

        } else { //One son - Just the left one
            vertex[i] = vertex[2*i];
            position[vertex[2*i]] = i;
            i = 2*i;
        }

    }

    //insert the end node in the place of i
    vertex[i] = vertex[size-1];
    position[vertex[size-1]] = i;
    G->heap->size--;

    //Minus 1 for all min neighboors
    for (uint32_t v=G->cd[min]; v<G->cd[min+1]; v++) {
        degree[G->adj[v]]--;
    }

    uint32_t u, p;
    //New position for all min neighboors
    for (uint32_t v=G->cd[min]; v<G->cd[min+1]; v++) {
        u = G->adj[v];
        if (!mark_vertex[u]) {
            i = position[u];

            while (i > 1) {
                if (degree[vertex[i]] < degree[vertex[i/2]]) {
                    p = vertex[i];
                    vertex[i] = vertex[i/2];
                    vertex[i/2] = p;
                    position[vertex[i]] = i/2;
                    position[vertex[i/2]] = i;
                    i = i/2;
                } else {
                    break;
                }
            }
        }

    }

    return min;
}

uint32_t* k_core_decomposition(adjarray_t* G) {

    uint32_t* core_value = (uint32_t*) malloc(G->n * sizeof(uint32_t));
    char* mark_vertex = (char*) calloc(G->n, sizeof(char));
    uint32_t n = G->n;
    uint32_t* eta = (uint32_t*) malloc(G->n * sizeof(uint32_t));

    uint32_t i = G->n;
    uint32_t c = 0;
    uint32_t v, dv;

    while (n > 0) {

        //Find the node with the minimum degree in G
        v = find_min(G, mark_vertex);
        dv = G->heap->degree[v];
        //printf("v = %u, dv = %u\n", v, dv);

        //c = max(c, d(u))
        if (c < dv) c = dv;

        //Save the core value of v
        core_value[v] = c;

        //V(G) <-- V(G) \ {v}
        mark_vertex[v] = 1;
        n--;

        //Eta(v) = i; i = i-1
        eta[v] = i;
        i--;
    }

    free(mark_vertex);
    free(eta);

    return core_value;
}


int main(int argc, char** argv) {

    if (argc != 3) {
        perror("./k_core_decomposition nom_du_fichier");
        exit(EXIT_FAILURE);
    }

    adjarray_t* G = load_graph(argv[1], argv[2]);
    printf("Load success ...\n");

    clock_t begin = clock();
    uint32_t* kcd = k_core_decomposition(G);
    printf("Temps de calcul = %lf\n", ((double) clock() - begin)/CLOCKS_PER_SEC);

    del_graph(G);
    return 0;
}