#include "myGraph.h"

adjarray_t* load_graph(const char* fname, const char* fname2) {

	FILE* f = fopen(fname, "r");

	uint32_t u, v, n = 0, e = 0;
	edge_t* edges = (edge_t*) malloc(sizeof(edge_t) * NLINKS);
	uint32_t m = NLINKS;

	while (fscanf(f, "%u %u", &u, &v) == 2) {
		if (e == m) {
			m += NLINKS/10;
			edges = (edge_t*) realloc(edges, sizeof(edge_t) * m);
		}
		edges[e].u1 = u;
		edges[e].u2 = v;
		if (n < u) n = u;
		if (n < v) n = v;
		e++;
	}

	n++;

	uint32_t* cd = (uint32_t*) calloc(n+1, sizeof(uint32_t));
	uint32_t* d = (uint32_t*) calloc(n+1, sizeof(uint32_t));

	for (uint32_t i = 0; i < e; i++) {
		d[edges[i].u1]++;
		d[edges[i].u2]++;
	}
	for (uint32_t i = 1; i < n+1; cd[i] += cd[i-1] + d[i-1], i++);

	uint32_t* d2 = (uint32_t*) calloc(n, sizeof(uint32_t));
	uint32_t* adj = (uint32_t*) malloc(cd[n] * sizeof(uint32_t));

	for (uint32_t i=0; i<e; i++) {
		u = edges[i].u1;
		v = edges[i].u2;
		adj[cd[u]+d2[u]++] = v;
		adj[cd[v]+d2[v]++] = u;
	}

	free(edges);
	free(d2);
	fclose(f);

	f = fopen(fname2, "r");
	min_heap_t* heap = (min_heap_t*) malloc(sizeof(min_heap_t));
	heap->vertex = (uint32_t*) malloc(sizeof(uint32_t)*(n+1));
	heap->position = (uint32_t*) malloc(sizeof(uint32_t)*n);
	heap->degree = d;
	heap->size = 1;

	while (fscanf(f, "%u", &u) == 1) {
		heap->vertex[heap->size] = u;
		heap->position[u] = heap->size;
		heap->size++;
	}

	fclose(f);


	adjarray_t* G = (adjarray_t*) malloc(sizeof(adjarray_t));
	G->n = n;
	G->cd = cd;
	G->adj = adj;
	G->heap = heap;
	return G;
}

void del_graph(adjarray_t* G) {
	free(G->cd);
	free(G->adj);
	free(G->heap->degree);
	free(G->heap->position);
	free(G->heap->vertex);
	free(G);
}
