#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "adjarray.h"


int main(int argc, char** argv) {
	
	if (argc != 2) {
		perror("./load_adjarray nom_du_fichier");
		exit(EXIT_FAILURE);
	}
	
	FILE* file = fopen(argv[1], "r");
	
	if (!file) {
		perror("Erreur lors de l'ouverture du fichier");
		exit(EXIT_FAILURE);
	}
	
	adjlist* g = readedgelist(file);
	
	mkadjlist(g);
	
	free(g);
	
	return 0;
}
