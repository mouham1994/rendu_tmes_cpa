#ifndef __EDGELIST_H__
#define __EDGELIST_H__

#define NLINKS 100000000 //maximum number of edges for memory allocation, will increase if needed

typedef struct {
	unsigned long s;
	unsigned long t;
} edge;

//edge list structure:
typedef struct {
	unsigned long n;//number of nodes
	unsigned long e;//number of edges
	edge *edges;//list of edges
} edgelist;

//reading the edgelist from file
edgelist* readedgelist(FILE* file);

void free_edgelist(edgelist *g);

#endif
