#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "edgelist.h"


int main(int argc, char** argv) {
	
	if (argc != 2) {
		perror("./load_edgelist nom_du_fichier");
		exit(EXIT_FAILURE);
	}
	
	FILE* file = fopen(argv[1], "r");
	
	if (!file) {
		perror("Erreur lors de l'ouverture du fichier");
		exit(EXIT_FAILURE);
	}
	
	edgelist* g = readedgelist(file);
	
	free_edgelist(g);
	
	return 0;
}
