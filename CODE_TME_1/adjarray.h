#ifndef __ADJARRAY_H__
#define __ADJARRAY_H__

#define NLINKS 100000000 //maximum number of edges for memory allocation, will increase if needed

typedef struct {
	unsigned long s;
	unsigned long t;
} edge;

//edge list structure:
typedef struct {
	unsigned long n;//number of nodes
	unsigned long e;//number of edges
	edge *edges;//list of edges
	unsigned long *cd;//cumulative degree cd[0]=0 length=n+1
	unsigned long *adj;//concatenated lists of neighbors of all nodes
} adjlist;

//reading the edgelist from file
adjlist* readedgelist(FILE* file);

//building the adjacency matrix
void mkadjlist(adjlist* g);

//freeing memory
void free_adjlist(adjlist *g);

#endif
