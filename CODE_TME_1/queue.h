#ifndef __QUEUE_H__
#define __QUEUE_H__

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct queue_s {
    uint32_t max_size;
    uint32_t size;
    uint32_t begin;
    uint32_t end;
    uint32_t* buffer;
} queue_t;

queue_t* new_queue(uint32_t max_size);
void init_queue(queue_t* q);
void free_queue(queue_t* q);
int isEmpty_queue(queue_t* q);
int isFull_queue(queue_t* q);
void enqueue(queue_t* q, uint32_t data);
uint32_t dequeue(queue_t* q);

#endif
