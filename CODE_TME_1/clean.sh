#!/bin/sh
awk '{ if ($1 > $2) { print $2 " " $1 } else if ($1 < $2) { print $1 " " $2 } } ' $1 | sort -k1n,1n -k2n,2n | uniq > $2
