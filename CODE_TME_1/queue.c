#include "queue.h"

queue_t* new_queue(uint32_t max_size) {
	queue_t* q = (queue_t*) malloc(sizeof(queue_t));
	q->begin = 0;
	q->end = 0;
	q->size = 0;
	q->max_size = max_size;
	q->buffer = (uint32_t*) malloc(sizeof(uint32_t) * max_size);
	return q;
}

void init_queue(queue_t* q) {
	q->begin = 0;
	q->end = 0;
	q->size = 0;
}

void free_queue(queue_t* q) {
	free(q->buffer);
	free(q);
}

int isEmpty_queue(queue_t* q) {
	return q->size == 0;
}

int isFull_queue(queue_t* q) {
	return q->size == q->max_size;
}

void enqueue(queue_t* q, uint32_t data) {
	if (isFull_queue(q)) {
		printf("Full queue");
		fflush(stdout);
		exit(EXIT_FAILURE);
	}
	q->buffer[q->end] = data;
	q->end = (q->end + 1) % q->max_size;
	q->size++;
}

uint32_t dequeue(queue_t* q) {
	if (isEmpty_queue(q)) {
		printf("Empty queue");
		fflush(stdout);
		exit(EXIT_FAILURE);
	}
	uint32_t data = q->buffer[q->begin];
	q->begin = (q->begin + 1) % q->max_size;
	q->size--;
	return data;
}