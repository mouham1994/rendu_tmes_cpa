#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "myGraph.h"


uint32_t intersect(adjarray_t* g, uint32_t u, uint32_t v) {

    uint32_t du = g->cd[u+1] - g->cd[u];
    uint32_t dv = g->cd[v+1] - g->cd[v];

    uint32_t i=0, j=0, v_u, v_v;
    uint32_t nbre = 0;

    if ((u > g->adj[g->cd[u+1]-1]) ||
        (v > g->adj[g->cd[v+1]-1]) ||
        (v > g->adj[g->cd[u+1]-1]) ||
        (u > g->adj[g->cd[v+1]-1])) return 0;

    while (i < du && u > g->adj[g->cd[u]+i]) { i++; }
    while (j < dv && v > g->adj[g->cd[v]+j]) { j++; }

    while (i < du && j < dv) {
        v_u = g->adj[g->cd[u] + i];
        v_v = g->adj[g->cd[v] + j];
        if (v_u == v_v) {
            nbre++;
            i++;
            j++;
        } else if (v_u > v_v) {
            j++;
        } else {
            i++;
        }
    }
    return nbre;
}


uint32_t all_triangle(adjarray_t* g) {
    uint32_t nbre = 0;

    for (uint32_t u=0; u<g->n; u++) {
        for (uint32_t v=g->cd[u]; v<g->cd[u+1]; v++) {
            if (g->adj[v] > u) {
                nbre += intersect(g, u, g->adj[v]);
            }
        }
    }

    return nbre;
}


int main(int argc, char** argv) {

    if (argc < 2) {
        perror("./triangle nom_du_fichier");
        exit(EXIT_FAILURE);
    }

    adjarray_t* g = load_graph(argv[1]);
    printf("Load success ...\n");
    printf("Number of triangles = %u\n", all_triangle(g));



    del_graph(g);

    return 0;
}