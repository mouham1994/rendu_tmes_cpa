#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "queue.h"
#include "myGraph.h"

#define INFINI -1

uint32_t bfs_component(adjarray_t* g, char* mark, uint32_t s, queue_t* q) {

	mark[s] = 1;
	uint32_t taille = 0;
	enqueue(q, s);
	uint32_t u;

	while (!isEmpty_queue(q)) {
		u = dequeue(q);
		taille++;

		for (uint32_t v=g->cd[u]; v<g->cd[u+1]; v++) {
			if (!mark[g->adj[v]]) {
				mark[g->adj[v]] = 1;
				enqueue(q, g->adj[v]);
			}
		}
	}

	return taille;
}

uint32_t all_components(adjarray_t* g) {

	char* mark = (char*) malloc(sizeof(char) * g->n);
	for (uint32_t i=0; i<g->n; mark[i]=0, i++);
	queue_t* q = new_queue(g->n);
	uint32_t nbre = 0;

	for (uint32_t u=0; u<g->n; u++) {
		if (!mark[u]) {
			printf("Source = %u, Size = %u\n", u, bfs_component(g, mark, u, q));
			init_queue(q);
			nbre++;
		}
	}

	free(mark);
	free_queue(q);

	return nbre;
}


int32_t bfs_diameter(adjarray_t* g, uint32_t s, uint32_t* e, queue_t* q, int32_t* dist) {

	for (int64_t i=0; i<g->n; dist[i]=INFINI, i++);

	// Marquer la source comme visitée et l'enfiler
	dist[s] = 0;
	enqueue(q, s);

	uint32_t u;

	while (!isEmpty_queue(q)) {

		// Défiler un noeud
		u = dequeue(q);

		//Pour chaque voisin
		for (uint32_t v=g->cd[u]; v<g->cd[u+1]; v++) {
			if (dist[g->adj[v]] == INFINI) {
				dist[g->adj[v]] = dist[u] + 1;
				enqueue(q, g->adj[v]);
			}
		}

	}

	int32_t max = 0;
	for (uint32_t i=0; i<g->n; i++) {
		if (dist[i] > max) {
			max = dist[i];
			*e = i;
		}
	}

	return max;
}

int32_t diameter(adjarray_t* g, uint32_t s, int precision) {

	uint32_t e;
	int32_t diam, max=0;
	queue_t* q = new_queue(g->n);
	// Tableau pour les distances source --> dist[i]
	int32_t* dist = (int32_t*) malloc(sizeof(int32_t) * g->n);

	while (precision > 0) {
		diam = bfs_diameter(g, s, &e, q, dist);
		init_queue(q);
		if (diam <= max) return max;
		else max = diam;
		s = e;
		printf("Diametre = %d, Fin = %u\n", diam, e);
		precision--;
	}

	free_queue(q);
	free(dist);

	return max;
}

int main(int argc, char** argv) {

	if (argc < 2) {
        perror("./bfs nom_du_fichier");
        exit(EXIT_FAILURE);
    }

	adjarray_t* g = load_graph(argv[1]);
	printf("Load graph success ...\n");

	/*//Nombre de composantes :
	printf("Nombre de composantes = %u\n", all_components(g));
	*/

	//Diametre du graphe :
	printf("Diametre du graphe = %d\n", diameter(g, 0, 5));


	del_graph(g);

	return 0;
}