#ifndef __ADJMATRIX_H__
#define __ADJMATRIX_H__

#define NLINKS 100000000 //maximum number of edges for memory allocation, will increase if needed


typedef struct {
	unsigned long s;
	unsigned long t;
} edge;

//edge list structure:
typedef struct {
	unsigned long n;//number of nodes
	unsigned long e;//number of edges
	edge *edges;//list of edges
	char *mat;//adjacency matrix
} adjmatrix;

//reading the edgelist from file
adjmatrix* readedgelist(FILE* f);

//building the adjacency matrix
void mkmatrix(adjmatrix* g);

void free_adjmatrix(adjmatrix *g);

#endif
